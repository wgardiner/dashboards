# Dashboard Scripts

This is the hello world example for writing a dashboard script for the Our-Sci platform.

The goal of a dashboard is to visualize relevant data to a user.

The main entry for a new user is in the `src/index.js` file.

If you are new to the dashboard world, take a look at [./src/index.basics.js](./src/index.basics.js).

### Environment setup

(see the [Hello World Measurement-Script](https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world) example for further instructions).

First install NodeJS on your platform [https://nodejs.org](https://nodejs.org).

Then in this directory run

```bash
git clone git@gitlab.com:our-sci/macro-scripts.git
cd macro-scripts
npm install
```

### Running devserver

After installing the dependencies, try to run the example

```bash
npm run dev-server ./src/index.js
```

This will run the dev-server on port 9092 on localhost (adjust in [./webpack/dev-server.js](./webpack/dev-server.js)).

When done developing, you can upload the script to the our-sci web application on [https://app.our-sci.net/#/dashboard](https://app.our-sci.net/#/dashboard).

Make sure to update [./src/manifest.json](./src/manifest.json) before deploying.

### compiling your dashboard

```bash
npm run compile ./src/[your entry file here]
```

Output files are `./dist/bundle.js` and `./dist/manifest.json`.

### deploying from cmd line

```bash
npm run auth

# log into your account with our-sci

npm run compile ./src/[your entry file here]
npm run upload
```

After deploying the dashboard is available at `https://app.our-sci.net/dashboard/[your-dashboard-id]`

### Using VS Code

VS Code is a code editor [https://code.visualstudio.com/](https://code.visualstudio.com/).

With VS Code the web server can be started from the "Run Build Task ..." dialog.
Make sure you have the right file open, as it will be used to determine which entry point to use (i.e. ./src/index.js or any other script file you want to run).

If not passing anything else to the compile script it will use the last script file passed to the dev server. In doubt explicitly specify `npm run compile ./src/my-super-dashboard.js`.

Also, VS Code will prompt on startup to intall a couple of extensions to make life easier.

```bash
dbaeumer.vscode-eslint
esbenp.prettier-vscode
msjsdiag.debugger-for-chrome
```
