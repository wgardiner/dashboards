/**
 * This dashboard is a hello world example
 * To run in it, execute
 *
 * npm install # for updating dependencies
 * npm run dev-server ./src/index.js
 *
 * alternatively:
 * in vs-code just open the 'Run Build Task ...'
 * and select 'start developement webserver'
 * with this file open.
 *
 * then navigate to http://localhost:9092/
 *
 * make sure to install the recommended extensions
 *    "dbaeumer.vscode-eslint"
 *    "esbenp.prettier-vscode"
 *    "msjsdiag.debugger-for-chrome"
 *
 * As a shortcut, hit F5 to open chrome and
 * load the dashboard locally
 */
// eslint-disable no-await-in-loop

import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';
import * as Math from './lib/math';

export default dashboard;

const ID = 'build_Methods-Testing-Uses-reflectometer2-flat_1525715374';
const wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];

(async () => {
  dashboard.showLoading(true);

  dashboard.avatar();

  await dashboard.init();
  const res = await dashboard.loadSurvey(ID);
  console.log(res[ID]);

  dashboard.info('', 'Evaluation of Methods testing');

  for (let i = 0; i < res[ID].measurement.length; i += 1) {
    const url = res[ID]['measurement.meta.filename'][i];
    console.log(`loading url: ${url}`);
    if (!url) {
      break;
    }
    try {
      const val = await dashboard.loadMeasurement(url);
      const data = val.sample[0].data_raw;

      const counts = data.length / wavelengths.length;
      const medians = {};

      for (let k = 0; k < wavelengths.length; k += 1) {
        medians[wavelengths[k]] = Math.MathMEDIAN(data.slice(k * counts, (k + 1) * counts));
      }

      dashboard.table(medians, `median of ${res[ID].sample_id[i]}`);
    } catch (error) {
      console.log(`error fetching: ${error}`);
    }
  }

  dashboard.dataTable(ID);
  dashboard.showLoading(false);
})();
