/**
 * Welcome to the basics example
 * where some of the must know features for
 * dashboards are explained.
 *
 * To run this dashboard execute
 * npm run dev-server ./src/index.basic.js
 * or in vscode, select "Run Build Task ..." and
 * start development webserver
 *
 * and navigate to http://localhost:9092
 *
 */

import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard; // expose function to the browser window (via oursci variable)

/**
 * let's get started
 *
 * first let's import some data from a CSV file
 * Format needs to be in this structure
 *
 * id,color,value
 * 0,red,1.12414
 * 1,blue,2.333
 * 2,yellow,2
 *
 * Where the first line in the file is the column names separated by comma
 * and subsequent lines are the csv values
 */
const localData = require('../data/sample.csv');

/**
 * also let's assign that data an ID which we later use to reference
 */
const ID = 'local_csv_data';

/**
 * Next part is super important.
 *
 * the async block allows us to use the
 * 'await' keyword and allows to block while
 * waiting for an action to complete.
 *
 * The dashboard script must be wrapped in async
 * (async () => { HERE GOES THE CODE})();
 */

(async () => {
  dashboard.showLoading(true); // show the loading screen

  await dashboard.init(); // init the dashboard library, must always be called first

  /**
   * Next we load or feed the dashboard some data
   * use the await keyword to make sure not to skip ahead
   * if the action has async code.
   *
   * The returning result (res[ID]) will be structured in this way:
   *
   * {
   *    "id" : [0, 1, 2],
   *    "color" : ["red", "blue", "yellow"],
   *    "value" : [1.12414, 2.333, 2],
   * }
   */
  const res = await dashboard.feedCsv(ID, localData);

  console.log(res); // when in doubt, do a log
  console.log(res[ID]); // this is actually our csv data

  /**
   * Let's create some output
   */

  dashboard.info('Hello World', 'A simple card');

  /**
   * the column user has the user names, let's find all distinct users
   */
  const allUserNames = utils.distinct(res[ID].user);

  /**
   * let's create an message box with all the names
   */
  dashboard.info(allUserNames.join('<br>'), 'Here are all the users who contributed');

  /**
   * this following function prints a nice box with the statistical summary
   * of an array of numbers
   *
   * let's use this for the column TOC
   */
  dashboard.valueSummary(
    'Summary of Total Carbon Contents',
    res[ID].TOC,
    'note that some of the measurements are still pending',
    {
      wide: true,
    },
  );

  /**
   * Let's now check what we can do with the data
   *
   * first, how large is the table? (how many rows does the CSV have)
   */

  const rowCount = utils.count(res[ID]);
  dashboard.info(`Rows present in data: ${rowCount}`, 'Row Count');

  /**
   * let's only count now rows that have a value set for soil_color
   */

  const countWhereColorIsAnswered = utils.countIfAnswered(res[ID], 'soil_color');
  dashboard.warning('Count of rows where color was answered', countWhereColorIsAnswered);

  /**
   * let's show a simple table in the ui
   */

  const tableData = {
    'contributed by greg': utils.countDistinct(res[ID], 'user').greg,
    'contributed by manuel': utils.countDistinct(res[ID], 'user').manuel,
  };

  dashboard.table(tableData, 'Most important contributors');

  /**
   * Let's do the same but a bit more efficient
   */

  const moreConvenientUserData = utils.countDistinct(res[ID], 'user');
  dashboard.table(moreConvenientUserData, 'User contribution stats');

  /**
   * OK, now let's only show only show TOC values with
   *    color brown and
   *    vegetation > 20
   */
  const specifc = utils.filter(res[ID], (item) => {
    if (item.soil_color !== 'brown') {
      return false;
    }

    if (!item.vegetation) {
      return false;
    }

    const parsed = Number.parseFloat(item.vegetation);
    if (Number.isNaN(parsed)) {
      return false;
    }

    if (parsed <= 20) {
      return false;
    }

    return true;
  });

  dashboard.valueSummary('Summary of brown soil with vegetation > 20%', specifc.TOC, '', {
    wide: true,
  });

  /**
   * Many times it is necessary to group a set by an ID or type
   * The groupBy operation is exactly doing that and transforming
   * the data into a nested array, take a look at the transformation
   * below to see how the data is aranged
   */

  const groupedBySoilColor = utils.groupBy(res[ID], 'soil_color');
  console.log('new structure after grouping');
  console.log(groupedBySoilColor);

  /**
   * now lets plot a series by date collected and user collecting
   */

  const colorSeries = dashboard.initTimeSeries(
    groupedBySoilColor,
    'date',
    'TOC',
    row => `Soil Color: ${row.soil_color[0]}`,
  );

  dashboard.timeseries(colorSeries, 'TOC by soil color over Time collected');

  /**
   * last but not least, let's display a data table that makes data browsable
   */

  dashboard.dataTable(ID);

  dashboard.showLoading(false); // hide the loading screen
})();
