/**
 * This example explains how to parse URL parameters
 * and use them to alter the dashboard at runtime.
 *
 * For a general introduction, have a look at inde.js
 * or index.basics.js
 *
 * For testing, navigate to the url
 * http://localhost:9092/?color=red&vegetation=50
 *
 * after starting the dev server
 *
 * NOTE: if you are using vscode, make sure to run the dev server
 * with this file open
 *
 * When using NPM, run
 * npm run dev-server ./src/index.urlparams.js
 */

import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard; // expose function to the browser window (via oursci variable)

const localData = require('../data/sample.csv');

const ID = 'local_csv_data';

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();
  const res = await dashboard.feedCsv(ID, localData);

  const createLink = p => `<a href="${p}">${p}</a>`;
  const help = {
    'Set a color': createLink('./?color=tan'),
    'Set a minimum vegetation required': createLink('./?vegetation=20'),
    'Combine the two': createLink('./?color=red&vegetation=50'),
    'Additionally display summary of location': createLink('./?location=HM'),
    'Pass something invalid': createLink('./?vegetation=HELLO'),
  };

  dashboard.info(
    'This dashboard supports querying parameters, try it out',
    'Using Query Parameters with the Dashboard',
  );

  dashboard.table(help, 'Try out these links');

  console.log(res[ID]); // this is actually our csv data

  /**
   * utils.param will try to find the param in the url
   * such as in below example the color
   *
   * http://localhost:9092/?color=red
   *
   * the second argument of the function specifies the default value
   * in this case 'brown'
   */
  const colorArg = utils.param('color', 'brown');
  const minVegetation = Number.parseFloat(utils.param('vegetation', '0'));
  const sampleLocation = utils.param('location', '');

  if (Number.isNaN(minVegetation)) {
    dashboard.error(`vegetation parameter is not a valid number:  ${minVegetation}`);
    dashboard.showLoading(false);
    return;
  }

  console.log(`color argument is: ${colorArg}`);
  const filtered = utils.filter(res[ID], (item) => {
    if (!item.vegetation) {
      return false;
    }

    if (!item.soil_color) {
      return false;
    }

    if (item.vegetation < minVegetation) {
      return false;
    }

    if (!colorArg) {
      return true;
    }

    if (item.soil_color !== colorArg) {
      return false;
    }

    return true;
  });

  if (utils.countIfAnswered(filtered, 'soil_color') === 0) {
    dashboard.error(`Unable to find any samples with soil_color ${colorArg} and minimum vegetation of ${minVegetation.toPrecision(3)}%`);
  } else {
    dashboard.valueSummary(
      `TOC of soil with color: ${colorArg} and minimum vegetation of ${minVegetation.toPrecision(3)}%`,
      filtered.TOC,
      `Note that the total count of measurements is ${utils.count(res[ID])}`,
      { wide: true },
    );
  }

  if (!sampleLocation) {
    dashboard.info('No sample location in URL provided, not displaying TOC for given location');
    dashboard.showLoading(false);
    return;
  }

  const samplesWithLocation = utils.filter(res[ID], (item) => {
    if (!item.sample_id_1) {
      return false;
    }

    if (item.sample_id_1.toLowerCase() !== sampleLocation.toLowerCase()) {
      return false;
    }

    return true;
  });

  if (samplesWithLocation.TOC.length === 0) {
    dashboard.error(`no samples found with location: ${sampleLocation}`);
    dashboard.showLoading(false);
  }

  dashboard.valueSummary(
    `TOC at sample location: ${sampleLocation}`,
    samplesWithLocation.TOC,
    `Total measurements ${utils.count(res[ID])}`,
    {
      wide: true,
    },
  );

  dashboard.showLoading(false);
})();
