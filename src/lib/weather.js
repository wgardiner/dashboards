import https from 'https';

const key = 'f8308119b5844e5faa7225518180706';

export default (lat, lon, from, to) =>
  new Promise((resolve, reject) => {
    let weather = '';

    https
      .get(
        `https://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=${key}&q=${lat},${lon}&format=json&date=${from}&enddate=${to}&tp=1`,
        (resp) => {
          resp.on('data', (chunk) => {
            weather += chunk;
          });

          resp.on('end', () => {
            try {
              const parsed = JSON.parse(weather);
              resolve(parsed);
            } catch (error) {
              reject(`error parsing answer from weather<br>${error}`);
            }
          });
        },
      )
      .on('error', (err) => {
        reject(`error connecting to weather<br>${err}`);
      });
  });
