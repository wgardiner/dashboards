import $ from 'jquery';
import 'chartist-plugin-fill-donut';
import 'chartist/dist/chartist.min.css';
import '../css/donut.css';

const Chartist = require('chartist');

export function card(
  title = 'Information',
  content = '<p>Card Content</p>',
  id,
  {
    backgroundColor = '#fff',
    foregroundColorTitle = '#000',
    foregroundColorContent = '#444',
    classes = 'col s12 m6',
  } = {},
) {
  let innerContent = '';
  if (content !== '' && content != null) {
    innerContent = `<span class="card-title" style="color: ${foregroundColorTitle}">${title}</span>
    <div class="plot-container"></div><div>${content}</div>`;
  } else {
    innerContent = `<span class="card-title" style="color: ${foregroundColorTitle}">${title}</span>
    <div class="plot-container"></div>`;
  }

  // let classNames = color.map((e) => material[e]).join(" ");

  return $(`<div id="${id}" class="${classes}"><div class="default-height">
      <div class="card" style="background-color: ${backgroundColor}">
        <div class="card-content" style="color: ${foregroundColorContent}">
         ${innerContent}
        </div>
      </div>
    </div></div>`);
}

export function donut(id, text, value, pct, icon = '') {
  const x1 = 220 * pct;
  const x2 = 220 * (1 - pct);

  Chartist.Pie(
    `#${id}`,
    {
      series: [x1, x2],
      labels: ['', ''],
    },
    {
      donut: true,
      donutWidth: 20,
      startAngle: 210,
      total: 260,
      showLabel: false,
      plugins: [
        Chartist.plugins.fillDonut({
          items: [
            {
              content: `<i>${icon}</i>`,
              position: 'bottom',
              offsetY: 10,
              offsetX: -2,
            },
            {
              content: value,
            },
          ],
        }),
      ],
    },
  );
}

export function dropdown(elements, callback) {
  const container = $('<div></div>');

  const options = elements.map(e => `<option value="${e}">${e}</option>`).join('');

  const select = $('<select></select>');
  select.addClass('browser-default');

  container.addClass('col s12');
  container.append(select);
  select.append($('<option value="" disabled selected>Choose your option</option>'));
  select.append($(`${options}`));
  container.append($('<label>Choose source for histogram'));

  select.change(() => {
    callback(select.val());
  });
  return container;
}
