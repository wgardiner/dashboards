import * as Plotly from 'plotly.js/lib/core';
import * as histogram from 'plotly.js/lib/histogram';
import * as bar from 'plotly.js/lib/bar';
import * as scatter from 'plotly.js/lib/scatter';

Plotly.register(histogram);
Plotly.register(bar);
Plotly.register(scatter);

export function sortTimeSeries(seriesx, seriesy) {
  const ret = {
    seriesx: [],
    seriesy: [],
  };
  seriesx
    .map((v, idx) => ({
      x: Date.parse(v),
      y: seriesy[idx],
    }))
    .sort((a, b) => {
      if (a.x === b.x) return 0;
      if (a.x < b.x) return -1;
      return 1;
    })
    .forEach((element) => {
      ret.seriesx.push(element.x);
      ret.seriesy.push(element.y);
    });

  return ret;
}

export function autohist(
  dom,
  series,
  titlex = '',
  titley = '',
  color = '#FFF',
  background = '#0000',
  nbins,
  gridColor,
  height = undefined,
) {
  Plotly.purge(dom);
  Plotly.plot(
    dom,
    [
      {
        x: series,
        type: 'histogram',
        marker: {
          color,
        },
        nbinsx: nbins,
      },
    ],
    {
      height,
      margin: {
        l: 40,
        r: 20,
        t: 20,
        b: 35,
      },
      bargap: 0.1,
      paper_bgcolor: background,
      plot_bgcolor: background,
      xaxis: {
        title: titlex,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
      yaxis: {
        title: titley,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
    },
  );
}

export function barchart(
  domElement,
  seriesx,
  seriesy,
  titlex = '',
  titley = '',
  color = '#FFF',
  background = '#0000',
  gridColor = color,
  height = undefined,
  gap = 0.1,
) {
  console.log(`using height: ${height}`);
  Plotly.purge(domElement);
  Plotly.plot(
    domElement,
    [
      {
        x: seriesx,
        y: seriesy,
        type: 'bar',
        marker: {
          color,
        },
      },
    ],
    {
      height,
      margin: {
        l: 40,
        r: 20,
        t: 20,
        b: 35,
      },
      bargap: gap,
      paper_bgcolor: background,
      plot_bgcolor: background,
      xaxis: {
        title: titlex,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
      yaxis: {
        title: titley,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
    },
  );
}

export function autoscatter(
  dom,
  series,
  titlex = '',
  titley = '',
  color = '#FFF',
  background = '#0000',
  height = undefined,
  gridColor = undefined,
) {
  Plotly.purge(dom);
  Plotly.plot(
    dom,
    [
      {
        x: series.x,
        y: series.y,
        type: 'scatter',
        mode: 'markers',
        marker: {
          color,
        },
      },
    ],
    {
      height,
      margin: {
        l: 40,
        r: 20,
        t: 20,
        b: 35,
      },
      bargap: 0.1,
      paper_bgcolor: background,
      plot_bgcolor: background,
      xaxis: {
        title: titlex,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
      yaxis: {
        title: titley,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
    },
  );
}

export function multitimeSeries(
  dom,
  seriesx,
  seriesy,
  labels,
  titlex = '',
  titley = '',
  color = '#FFF',
  background = '#0000',
  height = undefined,
  gridColor = undefined,
) {
  Plotly.purge(dom);
  Plotly.plot(
    dom,
    seriesx.map((s, idx) => {
      const sorted = sortTimeSeries(s, seriesy[idx]);

      return {
        x: sorted.seriesx,
        y: sorted.seriesy,
        type: 'scatter',
        marker: {
          color: idx === 0 ? color : undefined,
        },
        name: labels[idx],
      };
    }),
    {
      height,
      margin: {
        l: 40,
        r: 20,
        t: 20,
        b: 70,
      },
      paper_bgcolor: background,
      plot_bgcolor: background,
      xaxis: {
        title: titlex,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
        nticks: 5,
        type: 'date',
      },
      yaxis: {
        title: titley,
        titlefont: {
          color,
        },
        gridcolor: gridColor,
        tickfont: {
          color,
        },
      },
    },
  );
}

export function timeseries(
  dom,
  seriesx,
  seriesy,
  titlex = '',
  titley = '',
  color = '#FFF',
  background = '#0000',
  height = undefined,
  gridColor = undefined,
) {
  multitimeSeries(dom, [seriesx], [seriesy], titlex, titley, color, background, height, gridColor);
}
