/* global Tabletop */

import $ from 'jquery';
import { sprintf } from 'sprintf-js';
import url from 'url';
import * as path from 'path';
import jStat from 'jStat';

import Avatars from '@dicebear/avatars';
import SpriteCollection from '@dicebear/avatars-identicon-sprites';

import 'materialize-css/dist/js/materialize';
import 'materialize-css/dist/css/materialize.css';

import * as Plotly from 'plotly.js/lib/core';
import * as plotlyHistogram from 'plotly.js/lib/histogram';
import Chartist from 'chartist';
import Papa from 'papaparse';

import 'chartist/dist/chartist.min.css';
import 'chartist-plugin-fill-donut';

import 'datatables.net';
import 'datatables.net-dt/css/jquery.dataTables.css';

import mapsLoader from 'load-google-maps-api';
import MarkerClusterer from 'node-js-marker-clusterer';

import * as requestData from './requestdata';
import styles from '../css/style.css';
// import '../css/checkbox.css';

import * as OurPlotly from './ourplotly';
import * as ui from './ui';
// import manifest from '../manifest.json';

import useCacheHeader from './use-cache-header.html';

export { sprintf };

let gmaps = null;
export const exampleLocations = [];

Plotly.register(plotlyHistogram);

export const defaultForeground = '#01579b';
const cardColor = '#fff';
const errorColor = '#ef5350';
const warningColor = '#ff8f00';
const gridColor = `${defaultForeground}44`;

$('head').append(`<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
rel="stylesheet">`);

console.log(url.parse(window.location.href, true).query);

console.log(styles);
const loader = (() => {
  const l = $('<div></div>');
  l.attr('id', 'oursci-loader');
  l.html(` <div class="preloader-wrapper big active">  <div class="spinner-layer spinner-blue">
    <div class="circle-clipper left">
      <div class="circle"></div>
    </div><div class="gap-patch">
      <div class="circle"></div>
    </div><div class="circle-clipper right">
      <div class="circle"></div>
    </div>
  </div></div>`);
  return l;
})();

$('body').append(loader);

const cacheHeader = $(useCacheHeader);
const useCache = requestData.useCache();

if (useCache) {
  $('.filled-in', cacheHeader).attr('checked', 'checked');
}

$('.filled-in', cacheHeader).change((e) => {
  const checked = $(e.target).is(':checked');
  localStorage.setItem('cache', checked);
});

$('#script-container').append(cacheHeader);

const container = $('<div></div>');
container.attr('id', 'macro-container');
container.addClass('container row');
$('#script-container').append(container);

let cardCount = 0;
let themeColor = cardColor;

const cards = {};

/* eslint-disable import/no-mutable-exports */
const results = {};
/* eslint-enable import/no-mutable-exports */

export { results };

function transpose(a) {
  return a[0].map((_, c) => a.map(r => r[c]));
}

export function showLoading(visible) {
  if (visible) {
    $('#oursci-loader').show();
  } else {
    $('#oursci-loader').hide();
  }
}

/*
async function loadResults(...ids) {
  results = await requestData.fetchJson(ids);
  return results;
}
*/

function parseCsv(id, content) {
  results[id] = {};
  const parsed = Papa.parse(content[id]).data;
  const header = parsed[0];
  results[id] = {};

  header.forEach((h, colIdx) => {
    results[id][h] = parsed.filter((item, idx) => idx !== 0).map(r => r[colIdx]);
  });
}

export async function loadGooleSpreadsheet(id) {
  const docsUrl = `https://docs.google.com/spreadsheets/d/${id}/edit?usp=sharing`;

  if (useCache === true) {
    const cached = localStorage.getItem(docsUrl);
    if (cached !== null) {
      results[id] = JSON.parse(cached);
      return results;
    }
  }

  const d = await new Promise((resolve) => {
    Tabletop.init({
      key: docsUrl,
      callback: (data) => {
        resolve(data);
      },
      simpleSheet: true,
    });
  });

  results[id] = {};
  d.forEach((item) => {
    Object.keys(item).forEach((k) => {
      if (typeof results[id][k] === 'undefined') {
        results[id][k] = [];
      }
      results[id][k].push(item[k]);
    });
  });

  localStorage.setItem(docsUrl, JSON.stringify(results[id]));
  // parseCsv(response.data);
  return results;
}

export async function loadSurvey(...ids) {
  const loaded = await requestData.fetchCsv(ids);

  ids.forEach((id) => {
    parseCsv(id, loaded);
  });

  return results;
}

export async function loadMeasurement(measurementUrl) {
  return requestData.fetchMeasurement(measurementUrl);
}

export async function feedCsv(id, data) {
  const parsed = Papa.parse(data).data;
  const header = parsed[0];
  results[id] = {};
  header.forEach((h, colIdx) => {
    results[id][h] = parsed.filter((item, idx) => idx !== 0).map(r => r[colIdx]);
  });

  console.log(results);
  return results;
}

export function resLen(id) {
  try {
    const firstKey = Object.keys(results[id])[0];
    return results[id][firstKey].length;
  } catch (err) {
    return -1;
  }
}

function card(
  title,
  content,
  {
    backgroundColor = '#fff',
    foregroundColorTitle = '#000',
    foregroundColorContent = '#444',
    classes = 'col s12 m6',
  } = {},
) {
  const id = `card_${cardCount}`;
  cardCount += 1;
  const c = ui.card(title, content, id, {
    backgroundColor,
    foregroundColorTitle,
    foregroundColorContent,
    classes,
  });
  cards[id] = c;
  container.append(c);
  return id;
}

export function row(survey, column) {
  return results[survey][column];
}

export function mainColor(c) {
  themeColor = [c];
}

/**
 *
 * @param {string} message content inside the box
 * @param {string} title title of the box
 * @param {string} color the color of the background
 */
export function info(message, title = '', { color = cardColor, wide = false } = {}) {
  card(title, message, {
    backgroundColor: color,
    classes: wide ? 'col s12 m12' : 'col s12 m6',
  });
}

export function warning(message, title = '', color = warningColor, foregroundColor = '#FFF') {
  card(title, message, {
    backgroundColor: color,
    foregroundColorTitle: foregroundColor,
    foregroundColorContent: foregroundColor,
  });
}

export function error(message, title = '', color = errorColor, foregroundColor = '#FFF') {
  card(title, message, {
    backgroundColor: color,
    foregroundColorTitle: foregroundColor,
    foregroundColorContent: foregroundColor,
  });
}

export function timeseries(
  initializedSeries,
  title,
  message,
  {
    titlex = '',
    titley = '',
    foreground = defaultForeground,
    background = '#0000',
    wide = true,
  } = {},
  color = themeColor,
) {
  const c = card(title, message, {
    backgroundColor: color,
    classes: `col s12 ${wide ? 'm12' : 'm6'}`,
  });

  if (typeof initializedSeries.seriesx[0] === 'object') {
    OurPlotly.multitimeSeries(
      $(`#${c} .plot-container`)[0],
      initializedSeries.seriesx,
      initializedSeries.seriesy,
      initializedSeries.labels,
      titlex,
      titley,
      foreground,
      background,
      gridColor,
    );
  } else {
    OurPlotly.timeseries(
      $(`#${c} .plot-container`)[0],
      initializedSeries.seriesx,
      initializedSeries.seriesy,
      titlex,
      titley,
      foreground,
      background,
      gridColor,
    );
  }
}

export function initTimeSeries(obj, timeField, valueField, label = () => undefined) {
  return {
    seriesx: Array.isArray(obj) ? obj.map(rows => rows[timeField]) : [obj[timeField]],
    seriesy: Array.isArray(obj) ? obj.map(rows => rows[valueField]) : [obj[valueField]],
    labels: Array.isArray(obj) ? obj.map(item => label(item)) : [label(obj)],
  };
}

export function barchart(
  seriesx,
  seriesy,
  title,
  message,
  {
    titlex = '',
    titley = '',
    foreground = defaultForeground,
    background = '#0000',
    wide = false,
    height = undefined,
  } = {},
  color = themeColor,
) {
  const c = card(title, message, {
    backgroundColor: color,
    classes: `col s12 ${wide ? 'm12' : 'm6'}`,
  });

  OurPlotly.barchart(
    $(`#${c} .plot-container`)[0],
    seriesx,
    seriesy,
    titlex,
    titley,
    foreground,
    background,
    gridColor,
    height,
  );
}

export function histogram(
  series,
  title,
  message,
  {
    titlex = '',
    titley = '',
    foreground = defaultForeground,
    background = '#0000',
    nbins = 50,
    wide = false,
  } = {},
  color = themeColor,
) {
  const c = card(title, message, {
    backgroundColor: color,
    classes: `col s12 ${wide ? 'm12' : 'm6'}`,
  });

  OurPlotly.autohist(
    $(`#${c} .plot-container`)[0],
    series,
    titlex,
    titley,
    foreground,
    background,
    nbins,
  );
}

export function scatter(
  series,
  title,
  message,
  {
    titlex = '', titley = '', foreground = defaultForeground, background = '#0000',
  } = {},
  color = themeColor,
) {
  const c = card(title, message, {
    backgroundColor: color,
    classes: 'col s12 m12',
  });
  OurPlotly.autoscatter(
    $(`#${c} .plot-container`)[0],
    { x: series.x, y: series.y },
    titlex,
    titley,
    foreground,
    background,
    gridColor,
  );
}

/*
export function timeseries(values, timestamp) {}

export function barchart(series, labels) {}


export function donut(
  message = '',
  value = '',
  frac = 0.0,
  { foreground = defaultForeground, background = '#0000' } = {},
) {}

*/

function dropdownElement(q, checked) {
  return $(`<p><label class="checkbox-container">
    <input class="filled-in" id="chk-${q}" type="checkbox" ${
  checked ? 'checked="checked"' : ''
} "></input>
    <span class="checkmark">${q}</span>
    </label></p>`);
}

function tableColumns(id, ignore) {
  const dom = $(`<div>
      <div id="show-cols-${id}" class="button">
          Show/Hide Columns <i class="material-icons">expand_more</i>
      </div>
      <div class="column-dropdown" id="cols-${id}">
      </div>
  </div>`);

  Object.keys(results[id]).forEach((q) => {
    $(`#cols-${id}`, dom).append(dropdownElement(q, !ignore.includes(q)));
  });
  $(`#cols-${id}`, dom).hide();
  $(`#show-cols-${id}`, dom).click(() => {
    $(`#cols-${id}`, dom).slideToggle();
  });
  return dom;
}

export function dataTable(id, { ignoreDefault = [] } = {}) {
  const datatable = $('<table></table>');
  datatable.attr('id', 'results');
  // datatable.attr("width", "100%");
  datatable.attr('class', 'display compact');

  console.log(`datatable: ${datatable[0].outerHTML}`);
  card('Data Table', datatable[0].outerHTML, {
    classes: 'col s12 m12',
  });

  const resultAsArray = Object.values(results[id]);

  const columns = Object.keys(results[id]).map(k => ({
    title: k,
  }));

  const data = Object.keys(results[id]).map((k, questionIdx) => {
    if (k.endsWith('.meta.filename')) {
      return results[id][k].map((i, innerIdx) => {
        if (!i) {
          return '';
        }
        const linkname = path.basename(i);
        const scriptId = resultAsArray[questionIdx + 1][innerIdx];
        const filename = linkname;
        const instanceId = resultAsArray[1][innerIdx];
        const processorLink = `processor/${scriptId}?formId=${id}&instanceId=${instanceId}&filename=${filename}`;
        return `<a href="${i}" target="_blank">${decodeURIComponent(linkname)}</a> <a target="_blank" href="https://app.our-sci.net/${processorLink}"><i style="font-size: 14px" class="material-icons">open_in_new</i></a>`;
      });
    }
    return results[id][k].map((i) => {
      if (typeof i === 'undefined' || !i) {
        return '';
      }
      return i;
    });
  });

  tableColumns(id, ignoreDefault).insertBefore('#results');

  const dt = $('#results').DataTable({
    data: transpose(data),
    columns,
    scrollX: true,
  });

  $('.checkbox-container input').change((e) => {
    const colName = $(e.target)
      .next()
      .html();

    const idx = Object.keys(results[id]).indexOf(colName);
    console.log(`idx is ${idx}`);
    const visible = $(e.target).is(':checked');
    dt.column(idx).visible(visible);
    dt.draw();
  });

  ignoreDefault.forEach((colName) => {
    const idx = Object.keys(results[id]).indexOf(colName);
    dt.column(idx).visible(false);
  });
  // $('#results').before();
}

export function donut(
  message = '',
  title = '',
  pct = 1.0,
  innerLabel = '',
  {
    backgroundColor = themeColor,
    foregroundColor = defaultForeground,
    donutBackgroundColor = `${defaultForeground}22`,
  } = {},
) {
  const id = card(title, message, {
    backgroundColor,
    foregroundColorContent: foregroundColor,
    foregroundColorTitle: foregroundColor,
    classes: 'col s12 m6',
  });

  let actualPct = pct;

  if (actualPct > 1) {
    actualPct = 1;
  }

  if (actualPct < 0) {
    actualPct = 0;
  }

  const x1 = 7 * actualPct;
  const x2 = 7 * (1 - actualPct);

  // calculation for this
  // start angle: 220
  // diff to 180 => 220 - 180 = 40
  // double that to be symetric
  // 80 => leaves 360 - 80  = 280 deg
  // if max needs to be max frac 280/360
  // equals 7/9

  const chart = Chartist.Pie(
    $(`#${id} .plot-container`)[0],
    {
      series: [x1, x2],
      labels: ['', ''],
    },
    {
      donut: true,
      donutWidth: 20,
      startAngle: 220,
      total: 9,
      showLabel: false,
      plugins: [
        Chartist.plugins.fillDonut({
          items: [
            {
              content: '<i class="material-icons"></i>',
              position: 'bottom',
              offsetY: 10,
              offsetX: -2,
            },
            {
              content: innerLabel,
            },
          ],
        }),
      ],
    },
  );

  chart.on('created', () => {
    $(`#${id} .ct-series-a>path`).css('stroke', foregroundColor);
    $(`#${id} .ct-series-b>path`).css('stroke', donutBackgroundColor);
  });
}

function reRenderPlots() {
  const { d3 } = Plotly;
  const gd3 = d3.selectAll('.js-plotly-plot');
  const nodesToResize = gd3[0]; // not sure why but the goods are within a nested array

  for (let i = 0; i < nodesToResize.length; i += 1) {
    Plotly.Plots.resize(nodesToResize[i]);
  }
}

export async function map(
  message,
  title = '',
  height = '400px',
  locations = [],
  { backgroundColor = themeColor, foregroundColor = defaultForeground } = {},
) {
  const bounds = new gmaps.LatLngBounds();

  const markers = locations.map(loc =>
    new gmaps.Marker({
      position: loc.location,
    }));

  markers.forEach((marker) => {
    bounds.extend(marker.position);
  });

  const id = card(title, message, {
    backgroundColor,
    foregroundColorContent: foregroundColor,
    foregroundColorTitle: foregroundColor,
    classes: 'col s12 m12',
  });

  $(`#${id} .plot-container`).css('height', height);
  const m = new gmaps.Map($(`#${id} .plot-container`)[0], {
    zoom: 10,
    center: bounds.getCenter(),
  });

  m.setOptions({ maxZoom: 15 });

  markers.forEach((marker, idx) => {
    marker.addListener('click', () => {
      const w = new gmaps.InfoWindow({
        content: locations[idx].content,
      });

      w.open(m, marker);
    });
  });

  const listener = gmaps.event.addListener(m, 'idle', () => {
    console.log('bounds changed');
    gmaps.event.removeListener(listener);
    m.setOptions({ maxZoom: 100 });
  });

  m.fitBounds(bounds);

  const cluster = new MarkerClusterer(m, markers, {
    imagePath:
      'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
  });

  return { m, cluster };
}

export function pins(items, lat, lng, label = () => '', content = () => '') {
  return items[lat]
    .map((l, idx) => ({
      location: {
        lat: parseFloat(l),
        lng: parseFloat(items[lng][idx]),
      },
      label: label(items, idx),
      content: content(items, idx),
    }))
    .filter((item) => {
      if (Number.isNaN(item.location.lng) || Number.isNaN(item.location.lat)) {
        return false;
      }

      return true;
    });
}

export function surveyPins(items, locationField, label = () => '', content = () => '') {
  const latitudes = items[locationField].map(item => item.split(' ')[0]);
  const longitudes = items[locationField].map(item => item.split(' ')[1]);
  console.log(latitudes);

  return latitudes
    .map((l, idx) => ({
      location: {
        lat: parseFloat(l),
        lng: parseFloat(longitudes[idx]),
      },
      label: label(items, idx),
      content: content(items, idx),
    }))
    .filter((item) => {
      if (Number.isNaN(item.location.lng) || Number.isNaN(item.location.lat)) {
        return false;
      }

      return true;
    });
}

for (let i = 0; i < 10; i += 1) {
  const obj = {
    location: {
      lat: 47.4719201 + (Math.random() - 0.5) * 0.01,
      lng: 8.2988953 + (Math.random() - 0.5) * 0.01,
    },
    label: `${i}`,
    content: `this is important location ${i}`,
  };

  exampleLocations.push(obj);
}

export function valueSummary(title, series, message = '', { wide = false } = {}) {
  const answered = series.map(item => parseFloat(item)).filter((item) => {
    if (typeof item === 'undefined') {
      return false;
    }

    if (!item) {
      return false;
    }

    if (Number.isNaN(item)) {
      return false;
    }

    return true;
  });

  const count = `${answered.length} / ${series.length}`;
  const min = answered
    .reduce((acc, cur) => (cur < acc ? cur : acc), Number.MAX_VALUE)
    .toPrecision(5);
  const avg = answered.reduce((acc, cur) => acc + cur / answered.length, 0).toPrecision(5);
  const max = answered
    .reduce((acc, cur) => (cur > acc ? cur : acc), -Number.MAX_VALUE)
    .toPrecision(5);
  const stdev = jStat.stdev(answered).toPrecision(5);

  const values = {
    count,
    min,
    avg,
    max,
    stdev,
  };

  const inner = Object.keys(values)
    .map(k =>
      `<tr><td style="width: 90px">${k}</td><td style="text-align: left">${values[k]}</td></tr>`)
    .join('');

  const cardContent = `
  <div>
    <div class="wrapper" style="display: flex; margin-bottom: 20px;">
      <div class="barchart" style="width: 35%"></div>
      <div class="distribution" style="width: 65%"></div>
    </div>
    <div class="stats-table">
    <table>${inner}</table>
    <p><br>${message}</p>
    </div>
  </div>
  `;

  const c = card(title, cardContent, {
    classes: `col s12 ${wide ? 'm12' : 'm6'}`,
  });

  OurPlotly.barchart(
    $(`#${c} .barchart`)[0],
    ['min', 'avg', 'max'],
    [min, avg, max],
    'absolutes',
    undefined,
    defaultForeground,
    themeColor,
    gridColor,
    200,
    0.3,
  );

  OurPlotly.autohist(
    $(`#${c} .distribution`)[0],
    answered,
    'distribution',
    'count',
    defaultForeground,
    themeColor,
    20,
    gridColor,
    200,
  );
}

export function table(contentObj, title, { wide = false } = {}, color = themeColor) {
  const message = Object.keys(contentObj)
    .map(k => `<tr><td class="info-table-key">${k}</td><td>${contentObj[k]}</td></tr>`)
    .join('');
  card(title, `<table>${message}</table>`, {
    backgroundColor: color,
    classes: `col s12 ${wide ? 'm12' : 'm6'}`,
  });
}

export async function init() {
  const m = await mapsLoader({
    key: 'AIzaSyCczxPZdTwVvpMgUojaLx097D04wAXqJzY',
  });
  gmaps = m;

  await new Promise((resolve) => {
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/tabletop.js/1.5.1/tabletop.min.js', () =>
      resolve());
  });
}

(() => {
  window.onresize = () => {
    reRenderPlots();
  };
})();

function beforePrint() {
  reRenderPlots();
}

if (window.matchMedia) {
  const mediaQueryList = window.matchMedia('print');
  mediaQueryList.addListener((mql) => {
    if (mql.matches) {
      beforePrint();
    }
  });
}

window.onbeforeprint = beforePrint;

export function avatar() {
  const avatars = new Avatars(SpriteCollection);
  const svg = avatars.create('custom-seed');

  card('Avatar', svg, {
    backgroundColor: '#FFF',
    classes: 'col s12 m6',
  });
}
