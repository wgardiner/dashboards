import axios from 'axios';

export function useCache() {
  const cache = localStorage.getItem('cache');

  if (cache === null) {
    localStorage.setItem('cache', false);
    return false;
  }

  return cache === 'true';
}

export function enableCache(enable) {
  localStorage.setItem('cache', enable);
}

function buildTable(loaded) {
  console.log(loaded);

  const dataTable = {};
  const colmap = {};
  const columns = [];

  /** first fetch all column names */
  Object.keys(loaded).forEach((surveyName) => {
    const rows = loaded[surveyName];

    Object.values(rows).forEach((row) => {
      Object.keys(row).forEach((question) => {
        const answer = row[question];

        if (typeof answer === 'object' && typeof answer.data === 'object') {
          // it's a measurement
          Object.keys(answer.data).forEach((csvKey) => {
            const composite = `${question}.${csvKey}`;
            if (!columns.includes(composite)) {
              colmap[composite] = [question, csvKey];
              columns.push(composite);
            }
          });
        } else if (!columns.includes(question)) {
          // not an object
          columns.push(question);
        }
      });
    });
  });

  Object.keys(loaded).forEach((surveyName) => {
    dataTable[surveyName] = {};

    const rows = loaded[surveyName];

    Object.values(columns).forEach((col) => {
      const currentCol = [];

      Object.values(rows).forEach((row) => {
        if (Object.keys(row).includes(col)) {
          // not a measurement
          currentCol.push(row[col]);
          return;
        }
        // a measurement

        try {
          const [question, csvKey] = colmap[col];
          if (Object.prototype.hasOwnProperty.call(row[question], 'data')) {
            currentCol.push(row[question].data[csvKey]);
          } else {
            currentCol.push(undefined);
          }
        } catch (err) {
          currentCol.push(undefined);
        }
      });

      dataTable[surveyName][col] = currentCol;
    });
  });

  return dataTable;
}

export async function fetchJson(ids) {
  const loaded = {};

  /**
  if (typeof OFFLINE === "boolean" && OFFLINE == true) {
    ids.forEach(id => {
      loaded[id] = Object.values(require(`../../mock/${id}.json`));
    });
  } else {

  }
  */

  await Promise.all(ids.map(async (id) => {
    try {
      const url = `https://app.our-sci.net/api/survey/result/keys-and-values/by-form-id/${id}`;
      if (useCache()) {
        const cached = localStorage.getItem(url);
        if (cached != null) {
          loaded[id] = cached;
          return;
        }
      }
      const r = await axios(url);
      localStorage.setItem(url, JSON.stringify(r.data));
      loaded[id] = r.data;
    } catch (error) {
      console.log(`error fetching: ${error}`);
    }
  }));

  return buildTable(loaded); // TODO, what if we are online?
}

export async function fetchCsv(ids) {
  const loaded = {};

  await Promise.all(ids.map(async (id) => {
    try {
      const url = `https://app.our-sci.net/api/survey/result/csv/${id}`;

      if (useCache()) {
        const cached = localStorage.getItem(url);
        if (cached != null) {
          loaded[id] = JSON.parse(cached);
          return;
        }
      }

      const r = await axios(url);
      localStorage.setItem(url, JSON.stringify(r.data));
      loaded[id] = r.data;
    } catch (error) {
      console.log(`error fetching: ${error}`);
    }
  }));

  return loaded;
}

export async function fetchMeasurement(measurementUrl) {
  if (useCache === true) {
    const cached = localStorage.getItem(measurementUrl);
    if (cached !== null) {
      return cached;
    }
  }

  const res = await axios(measurementUrl);
  localStorage.setItem(measurementUrl, res.data);
  return res.data;
}
