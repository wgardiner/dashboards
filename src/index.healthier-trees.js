/**
 * Welcome to the basics example
 * where some of the must know features for
 * dashboards are explained.
 *
 * To run this dashboard execute
 * npm run dev-server ./src/index.basic.js
 * or in vscode, select "Run Build Task ..." and
 * start development webserver
 *
 * and navigate to http://localhost:9092
 *
 */

import moment from 'moment';
import weather from './lib/weather';
import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard; // expose function to the browser window (via oursci variable)

const ID = 'Tree-Health-Survey_5';

const users = {
  eGUKWkezUgZSE9dWeucNXDhJ14k1: 'Greg Austic',
};

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();

  const res = await dashboard.loadSurvey(ID);

  console.log(res); // when in doubt, do a log
  console.log(res[ID]); // this is actually our csv data

  /**
   * Let's create some output
   */

  const allUserNames = utils.distinct(res[ID].metaUserID);
  dashboard.info(
    allUserNames.map(u => users[u]).join('<br>'),
    'Users who contributed to this survey',
  );

  dashboard.info(moment(res[ID].metaDateCreated[0]).fromNow(), 'Last Contribution');

  const pins = dashboard.surveyPins(res[ID], 'location');
  dashboard.map('Map of trees', 'Treemap', '600px', pins);

  try {
    let w;
    const date = res[ID]['chlorophyll1.meta.date'];
    const from = moment(date)
      .subtract(10, 'days')
      .format('YYYY-MM-DD');
    const to = moment(date).format('YYYY-MM-DD');

    const lat = res[ID].location[0].split(' ')[0];
    const lon = res[ID].location[0].split(' ')[1];

    if (window.localStorage.weather) {
      w = JSON.parse(window.localStorage.weather);
    } else {
      w = await weather(lat, lon, from, to);
      window.localStorage.weather = JSON.stringify(w);
    }

    const totalRain = [];
    const dates = [];
    console.log(w);
    w.data.weather.forEach((t, day) => {
      totalRain[day] = 0;
      dates.push(t.date);
      t.hourly.forEach((h) => {
        totalRain[day] += Number.parseFloat(h.precipMM);
      });
    });

    console.log(totalRain);

    dashboard.barchart(dates, totalRain, 'Rainfall over the past 11 days in [mm]', '', {
      wide: true,
    });
  } catch (error) {
    dashboard.error(error, 'error fetching weather data');
  }

  dashboard.valueSummary('SPAD summary', res[ID]['chlorophyll_1.data.chlorophyll_spad ']);
  dashboard.valueSummary('Carotenoid summary', res[ID]['chlorophyll_1.data.carotenoid_spad']);
  dashboard.valueSummary('SPAD Anthocyanin', res[ID]['chlorophyll_1.data.anthocyanin_spad']);

  dashboard.dataTable(ID);
  dashboard.showLoading(false);
})();
