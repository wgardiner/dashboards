/**
 * This dashboard is a hello world example
 * To run in it, execute
 *
 * npm install # for updating dependencies
 * npm run dev-server ./src/index.js
 *
 * alternatively:
 * in vs-code just open the 'Run Build Task ...'
 * and select 'start developement webserver'
 * with this file open.
 *
 * then navigate to http://localhost:9092/
 *
 * make sure to install the recommended extensions
 *    "dbaeumer.vscode-eslint"
 *    "esbenp.prettier-vscode"
 *    "msjsdiag.debugger-for-chrome"
 *
 * As a shortcut, hit F5 to open chrome and
 * load the dashboard locally
 */
import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard;

// https://docs.google.com/spreadsheets/d/1rSekd3o8v2oOoT8QoAxw6_Y4J7XOZFK8N0IMo3HiNsY/edit?usp=sharing
const ID = '1rSekd3o8v2oOoT8QoAxw6_Y4J7XOZFK8N0IMo3HiNsY';

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();

  const res = await dashboard.loadGooleSpreadsheet(ID);

  const samplesTakenWhereTocIsMeasured = utils.countIfAnswered(res[ID], 'TOC');
  const target = 782;

  dashboard.donut(
    `${samplesTakenWhereTocIsMeasured} have been taken where TOC was measured out of targeted
     ${target} goal.`,
    'Overall Project Progress',
    samplesTakenWhereTocIsMeasured / target,
    `${(samplesTakenWhereTocIsMeasured / target * 100).toPrecision(3)} %`,
  );

  dashboard.info('welcome to our dashboard');

  dashboard.info(
    `Dataset Count, collected by ${utils.distinct(res[ID].user).join(', ')}`,
    Object.keys(res[ID]).length,
  );

  const collectedByDan = utils.count(utils.filter(res[ID], item => item.user === 'dan'));
  const ratio = collectedByDan / utils.count(res[ID]);

  dashboard.donut(
    `Dans contribution: ${collectedByDan} samples of ${utils.count(res[ID])}`,
    'Collected by Dan',
    ratio,
    collectedByDan,
  );

  dashboard.info(
    Object.keys(res[ID]).length,
    `Dataset Count, collected by ${utils.distinct(res[ID].user).join(', ')}`,
  );

  const latest = utils.zip(res[ID]).reduce((prev, next) => {
    if (!next.date) {
      // date was not answered
      return prev;
    }

    if (next.date > prev.date) {
      return next;
    }

    return prev;
  });

  dashboard.info(
    `Latest contribution was by ${latest.user} on ${latest.date}`,
    'Latest Contribution',
  );

  const contributionsByUsers = utils.countDistinct(res[ID], 'user');

  dashboard.table(contributionsByUsers, 'Contributions by users');

  dashboard.valueSummary(
    'Summary of vegetation',
    res[ID].vegetation,
    'Notice that a large max has implications of overal estimation of the model',
    {
      wide: true,
    },
  );
  dashboard.valueSummary('Summary of moisture', res[ID].moisture);
  dashboard.valueSummary('Summary of TOC', res[ID].TOC);

  utils.distinct(res[ID].soil_color).forEach((c) => {
    const filtered = utils.filter(res[ID], item => item.soil_color === c);

    dashboard.valueSummary(`Summary of TOC (color ${c})`, filtered.TOC);
  });

  utils.distinct(res[ID].sample_id_1).forEach((c) => {
    const filtered = utils.filter(res[ID], item => item.sample_id_1 === c);

    dashboard.valueSummary(`Summary of TOC (Sample ID ${c})`, filtered.TOC);
  });

  const series = utils.filter(res[ID], item => item.soil_color === 'red');

  dashboard.scatter(
    {
      x: series.TOC,
      y: series.moisture,
    },
    'TOC vs Moisture for Red Soil',
    'Diagram shows correlation between TOC and moisture for redish soil',
    {
      titlex: 'TOC',
      titley: 'Moisture (VWC)%',
    },
  );

  const filteredByUser = utils.filter(res[ID], item => item.user === 'manuel');
  console.log(filteredByUser);

  const initializedTimeSeries = dashboard.initTimeSeries(filteredByUser, 'date', 'vegetation');
  console.log(initializedTimeSeries);

  dashboard.timeseries(
    initializedTimeSeries,
    'vegetation over time for samples collected by manuel',
    '',
    {
      titley: 'vegetation coverage in %',
      titlex: 'time measurement was taken',
      wide: true,
    },
  );

  const pins = dashboard.pins(
    utils.filter(res[ID], item => item.user === 'greg'),
    'lat',
    'lon',
    (results, idx) => results.sample_ID_combined[idx],
    (results, idx) => `Total Carbon is ${Number.parseFloat(results.TOC[idx]).toPrecision(4)}%,
      sample was collected in ${results.sample_id_1[idx]}`,
  );

  dashboard.map('Measurements taken by greg', 'Map of Measurements taken by Greg', '600px', pins);

  dashboard.dataTable(ID, {
    ignoreDefault: ['metaInstanceID', 'metaUserID', 'metaDate', 'moisture_measurement'],
  });

  /*
  dashboard.minMaxAvg('Statistics of moisture AC RMS', res[ID]['moisture_measurement.data.rms AC']);

  dashboard.timeseries(
    res[ID].metaDate,
    res[ID]['moisture_measurement.data.rms AC'],
    'RMS AC by date collected',
    'diagram showing RMV AC values by time collected',
    {
      titlex: 'Time of Collection',
      titley: 'Value of RMS AC',
      wide: true,
    },
  );

  const resultRows = dashboard.resLen(ID);

  // const columns = Object.keys(res[ID]).reduce((accum, cur) => `${accum + cur}<br>`, '');

  console.log('results loaded');
  dashboard.info(`Collected ${resultRows} soil Moisture measurements so far.`, 'Results');

  const rmsAnswered = res[ID]['moisture_measurement.data.rms AC'].reduce((acc, cur) => {
    if (typeof cur === 'undefined') {
      return acc;
    }

    return acc + 1;
  }, 0);

  const avgAnswered = res[ID]['moisture_measurement.data.avg'].reduce((acc, cur) => {
    if (typeof cur === 'undefined') {
      return acc;
    }

    return acc + 1;
  }, 0);

  dashboard.barchart(
    ['RMS Answered', 'AVG Answered'],
    [rmsAnswered, avgAnswered],
    'Amount of Answered questions',
    'note that not all questions may have been answered',
    { titlex: 'question', titley: 'Amount answered' },
  );

  dashboard.map('This is where all measurements have been taken.', 'Measurement Location', '50vw', [
    {
      location: {
        lat: 47.471861,
        lng: 8.307525,
      },
      content: 'Baden',
    },
  ]);

  dashboard.warning('some of the results are incomplete', 'warning');
  console.log(dashboard.row(ID, 'moisture_measurement.data.rms AC'));

  const rms = res[ID]['moisture_measurement.data.rms AC'];
  let unanswered = rms.reduce((accumulator, current) => {
    if (typeof current === 'undefined') {
      return accumulator + 1;
    }
    return accumulator;
  }, 0);

  const avgRms = rms.reduce((acc, cur) => {
    if (cur === undefined) {
      return acc;
    }

    return acc + cur / rms.length;
  }, 0.0);
  console.log(rms.length);

  dashboard.donut(
    'average value calculated over all taken measurements',
    'Average RMS value',
    0.3,
    dashboard.sprintf('%.2f', avgRms),
  );

  dashboard.histogram(
    rms,
    'Histogram of average RMS of AC value',
    `note that ${unanswered} results are still pending`,
    { titlex: 'RMS', titley: 'Amount of measurements inside' },
  );

  const avg = res[ID]['moisture_measurement.data.avg'];
  unanswered = avg.reduce((accumulator, current) => {
    if (typeof current === 'undefined') {
      return accumulator + 1;
    }
    return accumulator;
  }, 0);

  dashboard.histogram(
    avg,
    'Histogram of average Voltage',
    `note that ${unanswered} results are still pending`,
    { titlex: 'AVG in Volts', titley: 'Amount of measurements inside' },
  );

  const visual = res[ID].visual_indication;
  unanswered = visual.reduce((accumulator, current) => {
    if (!current) {
      return accumulator + 1;
    }
    return accumulator;
  }, 0);

  dashboard.histogram(
    visual,
    'Histogram of visual inspection',
    `note that ${unanswered} results are still pending`,
    {
      titlex: '0 = dry, 1 = medium moist, 2 = moist',
      titley: 'Amount of measurements inside',
      nbins: 3,
      wide: true,
    },
  );

  /*
  dashboard.dropdown(['moisture_measurement.avg', 'visual_indication'], (columnId) => {
    // HERE GOES THE CODE
    dashboard.histogram();
    dashboard.barchart();
  });
*/
  dashboard.showLoading(false);
})();
