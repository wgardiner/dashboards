/**
 * This dashboard is a hello world example
 * To run in it, execute
 *
 * npm install # for updating dependencies
 * npm run dev-server ./src/index.js
 *
 * alternatively:
 * in vs-code just open the 'Run Build Task ...'
 * and select 'start developement webserver'
 * with this file open.
 *
 * then navigate to http://localhost:9092/
 *
 * make sure to install the recommended extensions
 *    "dbaeumer.vscode-eslint"
 *    "esbenp.prettier-vscode"
 *    "msjsdiag.debugger-for-chrome"
 *
 * As a shortcut, hit F5 to open chrome and
 * load the dashboard locally
 */

import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard;

const ID = 'build_Soil-Moisture-Measure-soil-moisture-example_1519745049';

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();

  const res = await dashboard.loadSurvey(ID);

  const distinctSamples = utils.distinct(res[ID].sample_id);
  dashboard.info(distinctSamples.join('<br>'), 'Here are all the sample IDs');

  dashboard.valueSummary(
    'Summary of RMS AC values',
    res[ID]['moisture_measurement.data.rms AC'],
    'note that some of the measurements are still pending',
    {
      wide: true,
    },
  );
  dashboard.dataTable(ID);
  dashboard.showLoading(false);
})();
