/**
 * This dashboard is a hello world example
 * To run in it, execute
 *
 * npm install # for updating dependencies
 * npm run dev-server ./src/index.js
 *
 * alternatively:
 * in vs-code just open the 'Run Build Task ...'
 * and select 'start developement webserver'
 * with this file open.
 *
 * then navigate to http://localhost:9092/
 *
 * make sure to install the recommended extensions
 *    "dbaeumer.vscode-eslint"
 *    "esbenp.prettier-vscode"
 *    "msjsdiag.debugger-for-chrome"
 *
 * As a shortcut, hit F5 to open chrome and
 * load the dashboard locally
 */
import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard;

// https://docs.google.com/spreadsheets/d/1cIdadrH8tWwSCPAqsrYOu9MbGBnv5erzKyUTsr3l-N8/edit?usp=sharing
const ID = '1cIdadrH8tWwSCPAqsrYOu9MbGBnv5erzKyUTsr3l-N8';

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();

  const res = await dashboard.loadGooleSpreadsheet(ID);
  const sheet = res[ID];

  console.log(sheet);

  const treesById = utils.groupBy(sheet, 'Tree ID');
  const treeByIdSeries = dashboard.initTimeSeries(console.log(sheet);

  const treesById = utils.groupBy(sheet, 'Tree ID');
  const treeByIdSeries = dashboard.initTimeSeries(
    treesById,
    'Date',
    'SPAD',
    row => `Tree ${row['Tree ID'][0]}`,
  );

  console.log(treeByIdSeries);
  dashboard.timeseries(treeByIdSeries);

  dashboard.showLoading(false);
})();
