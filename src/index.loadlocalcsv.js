/**
 * In order to compile this, first start the dev server
 * inside vs code with this file open, or run
 * npm run dev-server ./src/index.localcsv.js
 *
 * then open a webbrowser at http://localhost:9092/
 */

import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard;

/**
 * load a local file
 */
const localData = require('../data/sample.csv');

const ID = 'local_csv_data';

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();

  const res = await dashboard.feedCsv(ID, localData);

  // dashboard.dataTable(ID);

  const samplesTakenWhereTocIsMeasured = utils.countIfAnswered(res[ID], 'TOC');
  const target = 782;

  dashboard.donut(
    `${samplesTakenWhereTocIsMeasured} have been taken where TOC was measured out of targeted
     ${target} goal.`,
    'Overall Project Progress',
    samplesTakenWhereTocIsMeasured / target,
    `${(samplesTakenWhereTocIsMeasured / target * 100).toPrecision(3)} %`,
  );

  dashboard.info('welcome to our dashboard');

  dashboard.info(
    `Dataset Count, collected by ${utils.distinct(res[ID].user).join(', ')}`,
    Object.keys(res[ID]).length,
  );

  const collectedByDan = utils.count(utils.filter(res[ID], item => item.user === 'dan'));
  const ratio = collectedByDan / utils.count(res[ID]);

  dashboard.donut(
    `Dans contribution: ${collectedByDan} samples of ${utils.count(res[ID])}`,
    'Collected by Dan',
    ratio,
    collectedByDan,
  );

  dashboard.info(
    Object.keys(res[ID]).length,
    `Dataset Count, collected by ${utils.distinct(res[ID].user).join(', ')}`,
  );

  const latest = utils.zip(res[ID]).reduce((prev, next) => {
    if (!next.date) {
      // date was not answered
      return prev;
    }

    if (next.date > prev.date) {
      return next;
    }

    return prev;
  });

  dashboard.info(
    `Latest contribution was by ${latest.user} on ${latest.date}`,
    'Latest Contribution',
  );

  const contributionsByUsers = utils.countDistinct(res[ID], 'user');

  dashboard.table(contributionsByUsers, 'Contributions by users');

  dashboard.valueSummary(
    'Summary of vegetation',
    res[ID].vegetation,
    'Notice that a large max has implications of overal estimation of the model',
    {
      wide: true,
    },
  );
  dashboard.valueSummary('Summary of moisture', res[ID].moisture);
  dashboard.valueSummary('Summary of TOC', res[ID].TOC);

  utils.distinct(res[ID].soil_color).forEach((c) => {
    const filtered = utils.filter(res[ID], item => item.soil_color === c);

    dashboard.valueSummary(`Summary of TOC (color ${c})`, filtered.TOC);
  });

  utils.distinct(res[ID].sample_id_1).forEach((c) => {
    const filtered = utils.filter(res[ID], item => item.sample_id_1 === c);

    dashboard.valueSummary(`Summary of TOC (Sample ID ${c})`, filtered.TOC);
  });

  const series = utils.filter(res[ID], item => item.soil_color === 'red');

  dashboard.scatter(
    {
      x: series.TOC,
      y: series.moisture,
    },
    'TOC vs Moisture for Red Soil',
    'Diagram shows correlation between TOC and moisture for redish soil',
    {
      titlex: 'TOC',
      titley: 'Moisture (VWC)%',
    },
  );

  const filteredByUser = utils.filter(res[ID], item => item.user === 'manuel');
  console.log(filteredByUser);

  const initializedTimeSeries = dashboard.initTimeSeries(filteredByUser, 'date', 'vegetation');
  console.log(initializedTimeSeries);

  dashboard.timeseries(
    initializedTimeSeries,
    'vegetation over time for samples collected by manuel',
    '',
    {
      titley: 'vegetation coverage in %',
      titlex: 'time measurement was taken',
      wide: true,
    },
  );
  const pins = dashboard.pins(
    utils.filter(res[ID], item => item.user === 'greg'),
    'lat',
    'lon',
    (results, idx) => results.sample_ID_combined[idx],
    (results, idx) => `Total Carbon is ${Number.parseFloat(results.TOC[idx]).toPrecision(4)}%,
      sample was collected in ${results.sample_id_1[idx]}`,
  );

  dashboard.map('Measurements taken by greg', 'Map of Measurements taken by Greg', '600px', pins);

  dashboard.dataTable(ID, {
    ignoreDefault: ['metaInstanceID', 'metaUserID', 'metaDate', 'moisture_measurement'],
  });

  dashboard.showLoading(false);
})();
