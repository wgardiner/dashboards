/*
 * This dashboard is a hello world example
 * To run in it, execute
 *
 * npm install # for updating dependencies
 * npm run dev-server ./src/index.js
 *
 * alternatively:
 * in vs-code just open the 'Run Build Task ...'
 * and select 'start developement webserver'
 * with this file open.
 *
 * then navigate to http://localhost:9092/
 *
 * make sure to install the recommended extensions
 *    "dbaeumer.vscode-eslint"
 *    "esbenp.prettier-vscode"
 *    "msjsdiag.debugger-for-chrome"
 *
 * As a shortcut, hit F5 to open chrome and
 * load the dashboard locally
 */
import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';

export default dashboard;

// https://docs.google.com/spreadsheets/d/1RtGKfoiqE0cUzztIe91ZZZKHCOt2oIaEPmHhANziaqE/edit?usp=sharing
const ID = '1RtGKfoiqE0cUzztIe91ZZZKHCOt2oIaEPmHhANziaqE';

(async () => {
  dashboard.showLoading(true);

  await dashboard.init();

  const res = await dashboard.loadGooleSpreadsheet(ID);

  console.log(res[ID].SPAD);

  dashboard.valueSummary(
    'Overall value summary of SPAD',
    res[ID].SPAD,
    'above values have been submitted by survey',
    { wide: true },
  );

  // dashboard.timeseries(singleTree.Date, singleTree.SPAD);

  const treesById = utils.groupBy(res[ID], 'Tree ID');
  const treeByIdSeries = dashboard.initTimeSeries(
    treesById,
    'Date',
    'SPAD',
    row => `Tree ${row['Tree ID'][0]}`,
  );

  console.log(treeByIdSeries);
  dashboard.timeseries(treeByIdSeries, 'SPAD over time', 'Tree health over time');

  const treeByIdSeriesphi2 = dashboard.initTimeSeries(
    treesById,
    'Date',
    'Phi2',
    row => `Tree ${row['Tree ID'][0]}`,
  );

  console.log(treeByIdSeriesphi2);
  dashboard.timeseries(treeByIdSeriesphi2, 'Phi2 over time', 'Photosynthesis over time');

  dashboard.scatter(
    {
      x: res[ID].Phi2,
      y: res[ID]['TF/TB'],
    },
    'Phi2 vs TF/TB',
    'The realationship between photosyntetic efficiency and the ratio of total fungi to total bacteria',
  );

  let minValue = Number.MAX_VALUE;
  res[ID].Phi2.forEach((item) => {
    if (item < minValue) {
      minValue = item;
    }
  });

  if (minValue < 0) {
    dashboard.error('Phi2 min value is < 0', `Phi min ${minValue}`);
  }

  dashboard.showLoading(false);

  dashboard.scatter(
    {
      x: res[ID].SPAD,
      y: res[ID].Ca,
    },
    'SPAD vs Ca',
    'all samples included',
  );

  const pins = dashboard.pins(res[ID], 'latitude', 'longitude');
  dashboard.map('Map of trees', 'Treemap', '600px', pins);


  dashboard.dataTable(ID, {
    ignoreDefault: ['metaInstanceID', 'metaUserID', 'metaDate', 'moisture_measurement'],
  });

  /*


  const measureCount = utils.count(res[ID]);
  dashboard.info('Tree Health Calculator', `Welcome, ${measureCount} measurements have been taken`);

  const healthCounts = utils.countDistinct(res[ID], 'Tree Condition');
  dashboard.donut(
    `${healthCounts.Fair} trees out of ${measureCount} have fair health`,
    'Overall Tree Health',
    healthCounts.Fair / measureCount,
    `${(healthCounts.Fair / measureCount * 100).toPrecision(3)} %`,
  );

  dashboard.table(utils.countDistinct(res[ID], 'Genus'), 'Measurements Genus');

  dashboard.valueSummary('SPAD values', res[ID].SPAD);
  dashboard.showLoading(false);

  console.log(utils.distinct(res[ID]['Tree ID']));

  const pins = dashboard.pins(
    res[ID],
    'latitude',
    'longitude',
    (results, idx) => results.SPAD[idx],
    (results, idx) => `SPAD value: ${Number.parseFloat(results.SPAD[idx]).toPrecision(4)}%,
      sample was collected on ${results.Date[idx]}`,
  );

  dashboard.map('SPAD values on map', 'Map of Trees measured', '600px', pins);

  dashboard.dataTable(ID);

  /*
  dashboard.info(
    `Dataset Count, collected by ${utils.distinct(res[ID].user).join(', ')}`,
    Object.keys(res[ID]).length,
  );

  const collectedByDan = utils.count(utils.filter(res[ID], item => item.user === 'dan'));
  const ratio = collectedByDan / utils.count(res[ID]);

  dashboard.donut(
    `Dans contribution: ${collectedByDan} samples of ${utils.count(res[ID])}`,
    'Collected by Dan',
    ratio,
    collectedByDan,
  );

  dashboard.info(
    Object.keys(res[ID]).length,
    `Dataset Count, collected by ${utils.distinct(res[ID].user).join(', ')}`,
  );

  const latest = utils.zip(res[ID]).reduce((prev, next) => {
    if (!next.date) {
      // date was not answered
      return prev;
    }

    if (next.date > prev.date) {
      return next;
    }

    return prev;
  });

  dashboard.info(
    `Latest contribution was by ${latest.user} on ${latest.date}`,
    'Latest Contribution',
  );

  const contributionsByUsers = utils.countDistinct(res[ID], 'user');

  dashboard.table(contributionsByUsers, 'Contributions by users');

  dashboard.valueSummary(
    'Summary of vegetation',
    res[ID].vegetation,
    'Notice that a large max has implications of overal estimation of the model',
    {
      wide: true,
    },
  );
  dashboard.valueSummary('Summary of moisture', res[ID].moisture);
  dashboard.valueSummary('Summary of TOC', res[ID].TOC);

  utils.distinct(res[ID].soil_color).forEach((c) => {
    const filtered = utils.filter(res[ID], item => item.soil_color === c);

    dashboard.valueSummary(`Summary of TOC (color ${c})`, filtered.TOC);
  });

  utils.distinct(res[ID].sample_id_1).forEach((c) => {
    const filtered = utils.filter(res[ID], item => item.sample_id_1 === c);

    dashboard.valueSummary(`Summary of TOC (Sample ID ${c})`, filtered.TOC);
  });

  const series = utils.filter(res[ID], item => item.soil_color === 'red');

  dashboard.scatter(
    {
      x: series.TOC,
      y: series.moisture,
    },
    'TOC vs Moisture for Red Soil',
    'Diagram shows correlation between TOC and moisture for redish soil',
    {
      titlex: 'TOC',
      titley: 'Moisture (VWC)%',
    },
  );

  const timeSeries = utils.filter(res[ID], item => item.user === 'manuel');
  dashboard.timeseries(
    timeSeries.date,
    timeSeries.vegetation,
    'vegetation over time for samples collected by manuel',
    '',
    {
      titley: 'vegetation coverage in %',
      titlex: 'time measurement was taken',
      wide: true,
    },
  );

  const pins = dashboard.pins(
    utils.filter(res[ID], item => item.user === 'greg'),
    'lat',
    'lon',
    (results, idx) => results.sample_ID_combined[idx],
    (results, idx) => `Total Carbon is ${Number.parseFloat(results.TOC[idx]).toPrecision(4)}%,
      sample was collected in ${results.sample_id_1[idx]}`,
  );

  dashboard.map('Measurements taken by greg', 'Map of Measurements taken by Greg', '600px', pins);

  dashboard.dataTable(ID, {
    ignoreDefault: ['metaInstanceID', 'metaUserID', 'metaDate', 'moisture_measurement'],
  });
*/
})();