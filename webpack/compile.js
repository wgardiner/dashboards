const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fsCli = require('fs-cli');
const entry = require('./entry.js').default;

const target = entry();

const externals = {
  tabletop: 'Tabletop',
};

const config = {
  entry: {
    app: ['babel-polyfill', target],
  },
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, '../dist/'),
    filename: 'bundle.js',
    libraryTarget: 'var',
    library: 'our',
    libraryExport: 'default',
  },
  externals,
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
          },
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ['url-loader?limit=10000&mimetype=application/font-woff'],
      },
      {
        test: /\.(ttf|eot|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ['file-loader'],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.js$/,
        use: 'ify-loader',
      },
      {
        test: /\.js$/,
        use: 'transform-loader?plotly.js/tasks/util/compress_attributes.js',
      },
      {
        test: /\.csv$/,
        use: 'raw-loader',
      },
      {
        test: /\.html$/,
        use: 'raw-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/template.html',
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
    }),
  ],
};

fsCli.cp('./src/manifest.json', './dist/manifest.json');

const compiler = webpack(config);
compiler.run((err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
    }
    return;
  }

  const info = stats.toJson();

  if (stats.hasErrors()) {
    console.error(info.errors);
  }

  if (stats.hasWarnings()) {
    console.warn(info.warnings);
  }
});
