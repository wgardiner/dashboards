const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackDevServer = require('webpack-dev-server');
const entry = require('./entry.js').default;

const target = entry();

const externals = {
  tabletop: 'Tabletop',
};
const config = {
  entry: {
    app: ['babel-polyfill', target],
  },
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'macroscript-compiled.js',
    libraryTarget: 'var',
    library: 'our',
    libraryExport: 'default',
  },
  externals,
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
          },
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ['url-loader?limit=10000&mimetype=application/font-woff'],
      },
      {
        test: /\.(ttf|eot|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ['file-loader'],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.js$/,
        use: 'ify-loader',
      },
      {
        test: /\.js$/,
        use: 'transform-loader?plotly.js/tasks/util/compress_attributes.js',
      },
      {
        test: /\.csv$/,
        use: 'raw-loader',
      },
      {
        test: /\.html$/,
        use: 'raw-loader',
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      OFFLINE: true,
    }),
    new HtmlWebpackPlugin({
      template: 'src/template.html',
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
};

const options = {
  contentBase: 'public',
  historyApiFallback: true,
  host: '127.0.0.1',
};

WebpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config);

const devServerOptions = Object.assign({}, options, {
  stats: {
    colors: true,
  },
});

// compiler.run((err, status) => {});
const server = new WebpackDevServer(compiler, devServerOptions);

server.listen(9092, '0.0.0.0', () => {
  console.log('Starting server on http://localhost:9092');
});
