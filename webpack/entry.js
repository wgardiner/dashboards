import fs from 'fs';
import fsCli from 'fs-cli';
import path from 'path';

export default function () {
  const configFile = path.join(__dirname, '.config.json');

  let target = path.join(__dirname, '..', 'src', 'index.js');

  if (fsCli.exists(configFile)) {
    try {
      const cfg = JSON.parse(fs.readFileSync(configFile));
      if (cfg.target) {
        ({ target } = cfg);
      }
    } catch (err) {
      console.error(err);
    }
  }
  const fileArg = process.argv.slice(2)[0];

  if (fileArg) {
    target = path.join(__dirname, '..', fileArg);
    console.log(`using entry file: ${target}`);
  } else {
    console.log(`file not specified, using last known target: ${target}`);
  }

  if (!fsCli.exists(target)) {
    console.error(`unable to find file: ${target}`);
    process.exit(1);
  }

  const expectedDir = path.join(__dirname, '..', 'src');
  if (expectedDir !== path.join(target, '..')) {
    console.error('\n\n\n');
    console.error(`unexpected target: ${target}`);
    console.error('if running in VS code make sure to open the proper file');
    console.error('i.e. ./src/index.js');
    console.error('before running the development server or compile script');
    console.error('\n\n\n');
    process.exit(1);
  }

  fs.writeFileSync(configFile, JSON.stringify({ target }));

  return target;
}
